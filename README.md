9:1 Teknik - Programering
===================
Idag så kommer ni att få lära er att programera i C#.
C# är ett programeringsspråk utvecklat av microsoft år 2000, 
ni kommer få lära er att skriva en simpel konsolapplikation.
Utvecklingsmiljön vi kommer använda finns [här](https://code.sololearn.com/#cs).



----------


Grundlägande syntax
----------

###Variabler:


----------


En varriabel är minnesadress som innehåller en bit av information, den informationen kan ta form av en:

 - String: En string är en tecken eller en grupp av tecken men
   även ifall tecknen är nummer så kan man inte utföra matematiska
   operationer på dom.
 
 - Int: En int kan lagra hela tal och kan användas för att utföra matematiska operationer.
 
 - Double: En double lagrar nummer med decimaler.
 
 - boolean: En bool kan lagra två olika värden, antingen true eller false.

I exemplet nedan representerar "x" namnet du vill namnge variabeln till:
```csharp
string x = "hej"; //deklarerar en string 
int x = 3; // deklarerar en int
bool x = true; // deklarerar en boolean
double x; // deklarerar en double
```


### Loops/Statments:


----------


Comparetors: 
Comparetors används för att jämföra två variabler och retunera en boolean.
```csharp
x == y // x är lika med y.
x <= y // x är mindre än eller lika med y.
x >= y // x är mer än eller lika med y.
x != y // x är inte lika med y.
x == y && x == y // x är lika med y och x är lika med y.
x == y || x == y // x är lika med y eller x är lika med y.
```
    
If-statement:
Ett if statement använder en Comparetor eller boolean för att bedömma ifall ett block av kod ska köras.
```csharp
if(Comparetor eller bool){
	// Kod
} else{
	// Kod
}
```
 While-loop:
 En While loop uprepar ett block av kod medans ett påstående är sant.
```csharp 	
while(Comparetor eller bool){
	// Kod
}
```
For-loop:
En for loop uprepar ett block av kod ett bestämt antal gånger.

```csharp
for(int i = 0; i <= antal gånger du vill uprepa koden; i++){
	// Kod
}
```

Metoder
----------

```csharp
Console.WriteLine("x"); // Skriver ut innehålet av en string till consol fönstret.
String x = Console.ReadLine(); //Retunerar vad användaren skriver i consolen till x.
Random random = New Random; int x = random.next(y); //genererar ett slumpmässigt nummer.
int x = Convert.ToInt32(y); // Försöker att konvertera en string representerat av x till en int.
```





















