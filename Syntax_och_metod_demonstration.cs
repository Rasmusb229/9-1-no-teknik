﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic_Syntax_Demonstartion
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            Console.WriteLine("Hej, Vad är ditt namn?");
            String namn = Console.ReadLine();
            Console.WriteLine($"Hej {namn}.");

            Console.WriteLine("Vill du gissa ett numer?");
            if (Console.ReadLine() == "ja")
            {
                Console.WriteLine("Gissa ett numer och så säger jag ifall det är högre eller lägre");
                int gissning;
                Random rand = new Random();
                int svar = rand.Next(100);
                for (int i = 0; i <= 20; i++)
                {
                    Console.WriteLine($"Runda: {i}");
                    gissning = Convert.ToInt32(Console.ReadLine());

                    if (gissning > svar)
                    {
                        //Större
                        Console.WriteLine("Din gissning är för stor.");
                    } else if (gissning < svar)
                    {
                        //Lägre
                        Console.WriteLine("Din gissning är för liten.");
                    } else
                    {
                        //Rätt
                        Console.WriteLine("Grattis, Du gissade rätt.");
                        Console.ReadLine();
                        
                        break;
                    }

                }
            } else
            {
                Console.WriteLine("Vad synd, hejdå.");
            }


        }
    }
}
